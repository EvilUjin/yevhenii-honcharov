public class User {

    private String firstName;
    private String surname;
    private String email;
    private String mobilePhone;
    private String workPhone;
    private String password;

    public User(String firstName, String surname, String email, String mobilePhone, String workPhone, String password) {

        this.firstName = firstName;
        setSurname(surname);
        setEmail(email);
        mobilePhone(mobilePhone);
        this.workPhone = workPhone;
        setPassword(password);
    }

    public User(String email, String password, String surname) {

        setEmail(email);
        setPassword(password);
        setSurname(surname);
        setmobilePhone(mobilePhone);
    }

    private void setmobilePhone(String mobilePhone) {
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        char[] charArray = email.toCharArray();
        boolean check = false;
        for (char c : charArray) {
            if (c == '@') {
                this.email = email;
                check = true;
                String resultSentence = getFirstName() + "'s email is - " + email;
                System.out.println(resultSentence);
            }
        }
        if (!check) {
            System.out.println("Your email should contain @");
        }
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if (password.length() >= 8 && password.length() <= 16) {
            String resultSentence = getFirstName() + "'s password is - " + password;
            System.out.println(resultSentence);
            this.password = password;
        } else {
            System.out.println("Your password should be minimum 8, max 16 symbols");
        }
    }

    public void setSurname(String surname) {
        if (surname.length() >= 2 && surname.length() <= 15) {
            System.out.println("Surname is correct for user " + getFirstName());
            this.surname = surname;
        } else {
            System.out.println("Your surname should be minimum 2, max 15 symbols");
        }
    }

    public void mobilePhone(String mobilePhone) {
        if (mobilePhone.length() == 13) {
            System.out.println("Mobile phone is correct for user " + getFirstName());
            this.mobilePhone = mobilePhone;
        } else {
            System.out.println("Your mobile phone should be 13 symbols");
        }

    }
}


