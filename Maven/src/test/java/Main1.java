public class Main1 {

    public static void main(String[] args) {

        User user1 = new User("Ievhenii", "Honcharov", "Mr.Meyser@mail", "+380500000", "12345", "Pas");
        User user2 = new User("Mr.Meyser@gmail.com", "Password1", "Honcharov");
        Offspring offspring = new Offspring("Alla", "Pugacheva", "AllaP@gmail.com", "3805011111111", "54321", "Password1",
                20000, 12);

        System.out.println(user1.getEmail());
        System.out.println(
                "Salary = " + offspring.changeSalary(offspring.getSalary(), offspring.getExperience()));
    }
}
